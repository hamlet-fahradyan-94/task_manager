<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        User::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $roleDeveloper = Role::where('name', 'developer')->first();
        $roleManager  = Role::where('name', 'manager')->first();

        $developerUser = User::create([
            'name' => 'Developer',
            'email' => 'developer@example.com',
            'password' => bcrypt('developer')
        ]);
        $developerUser->roles()->attach($roleDeveloper);

        $managerUser = User::create([
            'name' => 'Manager',
            'email' => 'manager@example.com',
            'password' => bcrypt('manager')
        ]);
        $managerUser->roles()->attach($roleManager);
    }
}
