<?php

use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Role::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $roles = [
            [
                'name' => 'developer',
                'display_name' => 'Developer',
                'description' => 'A Developer User'
            ],
            [
                'name' => 'manager',
                'display_name' => 'Manager',
                'description' => 'A Manager User'
            ]
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }
    }
}
