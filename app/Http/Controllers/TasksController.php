<?php

namespace App\Http\Controllers;

use App\Task;
use App\User;
use Illuminate\Http\Request;

class TasksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $task = null;

        if (!empty($request->developers)) {
            $request->request->add(['status_id' => 1]);
            $task = Task::create($request->all());
            $user = auth()->user();
            foreach ($request->developers as $developerId) {
                $user->tasks()->attach($task->id, ['user_id' => $user->id, 'assigned_user_id' => $developerId]);
            }
        } else {
            $request->request->add(['status_id' => null]);
            $task = Task::create($request->all());
        }

        return response()->json($task);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $task = null;

        if (!empty($request->developers)) {
            $task = Task::findOrFail($request->taskId);
            $task->update(['status_id' => 1]);
            $user = auth()->user();
            foreach ($request->developers as $developerId) {
                $user->tasks()->attach($task->id, ['user_id' => $user->id, 'assigned_user_id' => $developerId]);
            }
        }
        if ($request->status) {
            $task = Task::findOrFail($request->taskId);
            $task->update(['status_id' => $request->status]);
            $task->load('status');
        }

        return response()->json($task);
    }
}
