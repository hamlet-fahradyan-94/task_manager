<?php

namespace App\Http\Controllers;

use App\Status;
use App\Task;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = auth()->user();

        if ($user->hasRole('manager')) {
            $developers = User::whereHas('roles', function ($query) {
                $query->where('name', '=', 'developer');
            })->get();
            $unassignedTasks = Task::whereStatusId(null)->get();
            $assignedTasks = Task::whereHas('users', function ($query) use ($user) {
                $query->where('user_id', '=', $user->id);
            })->get();

            return view('home', compact(['developers', 'unassignedTasks', 'assignedTasks']));
        }
        else if ($user->hasRole('developer')) {
            $tasks = Task::whereHas('users', function ($query) use ($user) {
                $query->where('assigned_user_id', '=', $user->id);
            })->get();
            $tasks->load('status');
            $statuses = Status::where('name', '!=', 'assigned')->get();

            return view('home', compact(['tasks', 'statuses']));
        }
    }
}
