<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['title', 'deadline', 'description', 'status_id'];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function status()
    {
        return $this->hasOne('App\Status', 'id', 'status_id');
    }
}
