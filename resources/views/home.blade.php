@extends('layouts.app')

@section('content')
    <div>
        <div class="container">
            @role('manager')
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" id="open">
                Create Task
            </button>
            <form method="post" action="{{url('/task/create')}}" id="form">
            @csrf
            <!-- Modal -->
                <div class="modal" tabindex="-1" role="dialog" id="myModal">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="alert alert-danger" style="display:none"></div>
                            <div class="modal-header">
                                <h5 class="modal-title">Create new task</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="Title">Title :</label>
                                        <input type="text" class="form-control" name="title" id="title">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="Deadline">Deadline :</label>
                                        <input type="date" name="bday" max="3000-12-31"
                                               min="1000-01-01" class="form-control" name="deadline" id="deadline">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="Description">Description :</label>
                                        <textarea class="form-control" name="description" id="description"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="Developers">Assign :</label>
                                        <select class="selectpicker" multiple data-live-search="true" id="developers"
                                                name="developers">
                                            @foreach ($developers as $key => $value)
                                                <option value="{{ $value->id }}"
                                                >{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button class="btn btn-success" id="task-create">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <form method="put" action="{{url('/task/update')}}" id="form">
            @csrf
            <!-- Modal -->
                <div class="modal" tabindex="-1" role="dialog" id="myModal2">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="alert alert-danger" style="display:none"></div>
                            <div class="modal-header">
                                <h5 class="modal-title">Assign task</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="Developers">Developers:</label>
                                        <select class="selectpicker" multiple data-live-search="true"
                                                id="developers-update"
                                                name="developers-update">
                                            @foreach ($developers as $key => $value)
                                                <option value="{{ $value->id }}"
                                                >{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button class="btn btn-success" id="update-task">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="container mt-3">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#home">Unassigned tasks</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu1">Assigned tasks</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="home" class="container tab-pane active"><br>
                        <table class="table table-hover">
                            <thead>
                            <th>Title</th>
                            <th>Deadline</th>
                            <th>Description</th>
                            <th>Action</th>
                            </thead>
                            <tbody class="unassigned-tbody">
                            @foreach($unassignedTasks as $unassignedTask)
                                <tr>
                                    <td>{{$unassignedTask->title}} </td>
                                    <td>{{$unassignedTask->deadline}} </td>
                                    <td>{{$unassignedTask->description}} </td>
                                    <td>
                                        <button type="button" class="btn btn-secondary assign-task" data-toggle="modal"
                                                data-target="#myModal2" data-value="{{$unassignedTask->id}}">Assign task
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div id="menu1" class="container tab-pane fade"><br>
                        <table class="table table-hover">
                            <thead>
                            <th>Title</th>
                            <th>Deadline</th>
                            <th>Description</th>
                            </thead>
                            <tbody class="assigned-tbody">
                            @foreach($assignedTasks as $assignedTask)
                                <tr>
                                    <td>{{$assignedTask->title}} </td>
                                    <td>{{$assignedTask->deadline}} </td>
                                    <td>{{$assignedTask->description}} </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @endrole

            @role('developer')
            <form method="put" action="{{url('/task/update')}}" id="form">
            @csrf
            <!-- Modal -->
                <div class="modal" tabindex="-1" role="dialog" id="myModal2">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="alert alert-danger" style="display:none"></div>
                            <div class="modal-header">
                                <h5 class="modal-title">Change task status</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="Statuses">Statuses:</label>
                                        <select class="selectpicker" data-live-search="true" id="status-update"
                                                name="status-update">
                                            @foreach ($statuses as $key => $value)
                                                <option value="{{ $value->id }}"
                                                >{{ $value->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button class="btn btn-success" id="update-task-status">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <table class="table table-hover">
                <thead>
                <th>Title</th>
                <th>Deadline</th>
                <th>Description</th>
                <th>Status</th>
                <th>Action</th>
                </thead>
                <tbody class="task-tbody">
                @foreach($tasks as $task)
                    <tr>
                        <td>{{$task->title}} </td>
                        <td>{{$task->deadline}} </td>
                        <td>{{$task->description}} </td>
                        <td>{{$task->status->name}} </td>
                        {{--                            <td><a href='javascript:;' class="btn btn-secondary">Assign task</a></td>--}}
                        <td>
                            <button type="button" class="btn btn-secondary assign-task" data-toggle="modal"
                                    data-target="#myModal2" data-value="{{$task->id}}">Change status
                            </button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            @endrole
        </div>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
        <link rel="stylesheet"
              href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
        <script>
            $(document).ready(function () {
                let taskIdForUpdate = null;
                let taskForRemove = null;

                $('#task-create').click(function (e) {
                    e.preventDefault();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    let title = $('#title').val();
                    let deadline = $('#deadline').val();
                    let developers = $('#developers').val();

                    if (title && deadline) {
                        $.ajax({
                            url: "{{ url('/task/create') }}",
                            method: 'post',
                            data: {
                                title: title,
                                deadline: deadline,
                                description: $('#description').val(),
                                developers: developers,
                            },
                            success: function (result) {
                                if (!developers) {
                                    let str = '<tr><td>' + result.title +
                                        '</td><td>' + result.deadline + '</td><td>' + result.description +
                                        '</td><td>' +
                                        '<button type="button" class="btn btn-secondary assign-task new_participant_form" data-toggle="modal" data-target="#myModal2" data-value="'
                                        + result.id + '">Assign task</button>' + '</td></tr>';
                                    $(".unassigned-tbody").append(str);
                                } else {
                                    let str = '<tr><td>' + result.title +
                                        '</td><td>' + result.deadline + '</td><td>' + result.description +
                                        '</td><td>' + '</td></tr>';
                                    $(".assigned-tbody").append(str);
                                }

                                $('#title').val('');
                                $('#deadline').val('');
                                $('#description').val('');
                                $('#developers').val(null);
                                $('.filter-option-inner-inner').text('Nothing selected');

                                $('.alert-danger').hide();
                                $('#myModal').modal('hide');
                            }
                        });

                    }
                });
                $('#update-task').click(function (e) {
                    e.preventDefault();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "{{ url('/task/update') }}",
                        method: 'put',
                        data: {
                            taskId: taskIdForUpdate,
                            developers: $('#developers-update').val(),
                        },
                        success: function (result) {
                            let str = '<tr><td>' + result.title +
                                '</td><td>' + result.deadline + '</td><td>' + result.description +
                                '</td><td>' + '</td></tr>';

                            $(".assigned-tbody").append(str);

                            $('#developers-update').val(null);
                            $('.filter-option-inner-inner').text('Nothing selected');

                            $('.alert-danger').hide();
                            $('#myModal2').modal('hide');
                            taskForRemove.closest("tr").remove();
                        }
                    });
                });
                $('body').delegate('.assign-task', 'click', function (e) {
                    taskForRemove = $(this);
                    taskIdForUpdate = $(this).attr("data-value");
                });
                $('#update-task-status').click(function (e) {
                    e.preventDefault();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "{{ url('/task/update') }}",
                        method: 'put',
                        data: {
                            taskId: taskIdForUpdate,
                            status: $('#status-update').val(),
                        },
                        success: function (result) {
                            taskForRemove.closest("tr").children('td:eq(3)').html(result.status.name);
                            $('#status-update').val(null);
                            $('.filter-option-inner-inner').text('Nothing selected');

                            $('.alert-danger').hide();
                            $('#myModal2').modal('hide');
                        }
                    });
                });
            });
        </script>
    </div>
@endsection
